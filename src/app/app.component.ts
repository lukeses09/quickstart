import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
		<h2>{{title}}</h2>
		<div>
			<label>Name</label>
			<input [(ngModel)]="hero.name"  placeholder="Input Your Name" />
		</div>
	`
})
export class AppComponent  { 
	title = "Tour of Heroes";
	hero : Hero = {
		id: 1,
		name: `Windstorm`
	} 
}
